   
<form class="mainRicerca" method="POST">
    <div class="mainRicerca" id="elementiFormMainRicerca">
        <i class="fa fa-search" id="iconaMainRicercaRicerca" aria-hidden="true"></i>
        <div class="mainRicerca" id="inputMainRicerca">
            <label for="esame">Esame</label>
            <input type="text" name="esame" class="mainRicerca" id="mainRicercaEsame" target="_blank" 
                   placeholder="Raggi"/>
            <label for="luogo">Luogo</label>
            <input type="text" name="luogo" class="mainRicerca" id="mainRicercaLuogo" target="_blank" 
                   placeholder="Roma"/>
            <input type="submit" class="mainRicerca" id="mainRicercaCerca" value="Cerca">
        </div>
    </div>
</form>
