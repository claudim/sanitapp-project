<html>
    <head>
        <link rel="stylesheet" type="text/css" href="./Css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="./Css/logo.css">
        <link rel="stylesheet" type="text/css" href="./Css/homePage.css">
        <link rel="stylesheet" type="text/css" href="./Css/navigationBar.css">
        <link rel="stylesheet" type="text/css" href="./Css/mainRicerca.css">
        <link rel="stylesheet" type="text/css" href="./Css/inserisciUtente.css">
        <link rel="stylesheet" type="text/css" href="./Css/inserisciMedico.css">
        <link rel="stylesheet" type="text/css" href="./Css/footer.css">
        <link rel="stylesheet" type="text/css" href="./Css/cartinaItalia.css"/>
        <link rel="icon" href="./Immagini/favicon.ico" />
        <img src="./Immagini/cartinaItalia" />

        <!--inserisco meta viewport per ottenere una nav bar responsive-->
        <meta name="viewport" content="width=device-width" initial-scale=1  maximum-scale=1>    
        
        <meta charset="UTF-8">
        <script type="text/javascript" src="./jScript/jquery-1.12.4.min.js"></script>
        <script type="text/javascript" src="./jScript/adaptive-image.js"></script>
        <script type="text/javascript" src="./jScript/eventi_click.js"></script>
        <script type="text/javascript" src="./jScript/gestioneCartinaItalia.js"></script>
        <script type="text/javascript" src="./jScript/clickRegistrazione.js"></script>
        <script type="text/javascript" src="./jScript/mySanitappClick.js"></script>

        
    
    
    <body>
        <!-- Classe container per l'intera pagina-->
        <div id="wrapper">
            
            <!-- Header della pagina-->
            <div class="header" id="header">
                <!--<p id="logo">Sa<i class="fa fa-stethoscope fa-rotate-180" id="icona logo"></i>itApp</p> -->
                <img id="logoSanitApp" src="Immagini/logoSanitApp.png" alt="logoSanitApp">
            </div>
            
            <!-- Navigation bar della pagina-->
            <div class="nav" id="nav">
                {$navigationBar}             
            </div>    
            
            <!-- Main della pagina-->
            <div class= mainRicerca id="main">
                {$mainRicerca}
            </div> 
            
            <div id="contenutiAjax">
                
            </div>
            <!-- Footer della pagina-->
            <div id="footer">
                <span class="copyright">©2016 Copyright - SanitApp</span>
            </div>
        </div>
    </body>
</html>