        
<nav class="nav">
    <ul class="nav">
        <li class="nav" id="cliniche">    
            <a class="nav" href="#"><i class="fa fa-hospital-o" id="icona-cliniche" aria-hidden="true"></i> Cliniche</a> 
        </li>
        <li class="nav" id="esami">
            <a  class="nav"href="#">
                <i class="fa fa-file-text-o" id="icona-esami" aria-hidden="true"></i> Esami</a>
        </li>
        <li class="nav" id="home">
            <a class="nav" href="#">
                <i class="fa fa-home " id="icona-home" aria-hidden="true"></i> 
            </a>
        </li>
        <li class="nav" id="mySanitApp">
            <a class="nav" href="#"><i class="fa fa-user" id="icona-mySanitApp" aria-hidden="true"></i> MySanitApp</a>
        </li>
        <li class="nav" id="registrazione">
            <a class="nav" href="#"><i class="fa fa-user-plus" id="icona-registrazione" aria-hidden="true"></i> Registrazione</a>
            <div class="dropdown-content">
                <a id="registrazioneUtente" href="#">Utente</a>
                <a id="registrazioneMedico" href="#">Medico</a>
                <a id="registrazioneClinica" href="#">Clinica</a>
            </div>
        </li>
    </ul
    <!-- This extra link will be used to pull the menu navigation when it is hidden in a small screen-->
    <a href="#" id="pull"><i class="fa fa-bars" id="icona-menù" aria-hidden="true"></i>Menù</a>
</nav>
