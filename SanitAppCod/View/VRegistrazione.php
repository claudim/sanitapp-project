

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VRegistrazione
 *
 * @author Claudia Di Marco & Riccardo Mantini
 */
class VRegistrazione extends View {

    /**
     *  Metodo che permette di conoscere il valore di task dell'URL
     * 
     * @access public
     * @return mixed Ritorna il valore (stringa) di task. False altrimenti.
     */
    public function getTask() 
    {
        if (isset($_REQUEST['task'])) 
            {
                return $_REQUEST['task'];
            } 
        else 
            {
                return false;
            }
    }
    
    public function restituisciFormUtente() 
    {
        return $this->visualizzaTemplate('inserisciUtente');  
    }

    public function restituisciFormClinica() 
    {
        return $this->prelevaTemplate('inserisciClinica');
    }
    
    public function restituisciFormMedico() 
    {
        return $this->prelevaTemplate('inserisciMedico');
    }
}
