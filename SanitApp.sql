-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Creato il: Lug 07, 2016 alle 15:39
-- Versione del server: 10.1.10-MariaDB
-- Versione PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sanitapp`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `clinica`
--

CREATE TABLE `clinica` (
  `PartitaIVA` varchar(20) NOT NULL,
  `NomeClinica` varchar(20) NOT NULL,
  `Titolare` varchar(40) NOT NULL,
  `Via` varchar(20) NOT NULL,
  `NumCivico` smallint(6) DEFAULT NULL,
  `CAP` mediumint(7) UNSIGNED NOT NULL,
  `Email` varchar(100) NOT NULL ,
  `Username` varchar(10) NOT NULL,
  `Password` varchar(10) NOT NULL,
  `PEC` varchar(100) NOT NULL,
  `Telefono` smallint(6) DEFAULT NULL,
  `CapitaleSociale` int(11) DEFAULT NULL,
  `OrarioAperturaAM` time NOT NULL,
  `OrarioChiusuraAM` time ,
  `OrarioAperturaPM` time ,
  `OrarioChiusuraPM` time NOT NULL,
  `OrarioContinuato` boolean
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `clinica`
--

INSERT INTO `clinica` (`PartitaIVA`, `NomeClinica`, `Titolare`, `Via`, `NumCivico`, `CAP`, `Email`, `Username`, `Password`, `PEC`, `Telefono`, `CapitaleSociale`, `OrarioAperturaAM`,`OrarioChiusuraAM` ,`OrarioAperturaPM`,`OrarioChiusuraPM`, `OrarioContinuato`) VALUES
('12345', ' appignano', ' riccardo', ' del carmine', 2, 32767, ' info@appignano.it', ' appi', ' 1234', ' info@appignano.pec', 8612, 123456789, '08:00:00', '10:00:00', '15:00:00', '20:00:00', FALSE);

-- --------------------------------------------------------

--
-- Struttura della tabella `esame`
--

CREATE TABLE `esame` (
  `IDEsame` int(11) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Descrizione` varchar(200) DEFAULT NULL,
  `Prezzo` float NOT NULL,
  `Durata` smallint(6) NOT NULL,
  `MedicoEsame` varchar(40) NOT NULL,
  `NumPrestazioniSimultanee` smallint(6) NOT NULL,
  `NomeCategoria` varchar(30) NOT NULL,
  `PartitaIVAClinica` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `esame`
--

INSERT INTO `esame` (`IDEsame`, `Nome`, `Descrizione`, `Prezzo`, `Durata`, `MedicoEsame`, `NumPrestazioniSimultanee`, `NomeCategoria`, `PartitaIVAClinica`) VALUES
(1, 'raggi braccio', 'raggi al braccio', 30, 15, 'Riga', 1, 'Raggi', '12345'),
(2, 'raggi piede', 'raggi al piede', 30, 15, 'Riga', 1, 'Raggi\r\n', '12345');

-- --------------------------------------------------------

--
-- Struttura della tabella `medico`
--

CREATE TABLE `medico` (
  `CodFiscale` varchar(16) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Via` varchar(20) NOT NULL,
  `NumCivico` smallint(6) DEFAULT NULL,
  `CAP` mediumint(5) UNSIGNED NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Username` varchar(10) NOT NULL,
  `Password` varchar(10) NOT NULL,
  `PEC` varchar(100) NOT NULL,
  `Validato` tinyint(1) DEFAULT '0',
  `ProvinciaAlbo` varchar(2) NOT NULL,
  `NumIscrizione` smallint(6) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `medico`
--

INSERT INTO `medico` (`CodFiscale`, `Nome`, `Cognome`, `Via`, `NumCivico`, `CAP`, `Email`, `Password`, `PEC`, `Validato`, `ProvinciaAlbo`, `NumIscrizione`) VALUES
('dmrcld89s42g438s', ' claudia', ' di marco', ' acquaventina', 30, 32767, ' clau@hotmail.it', ' clau', ' clau@do.pec.it', 0, ' P', 5464);

-- --------------------------------------------------------

--
-- Struttura della tabella `prenotazione`
--

CREATE TABLE `prenotazione` (
  `IDPrenotazione` int(11) NOT NULL,
  `IDEsame` int(11) NOT NULL,
  `PartitaIVAClinica` varchar(20) NOT NULL,
  `Tipo` varchar(1) NOT NULL,
  `Confermata` tinyint(1) DEFAULT '0',
  `Eseguita` tinyint(1) DEFAULT '0',
  `CodFiscaleUtenteEffettuaEsame` varchar(16) NOT NULL,
  `CodFiscaleMedicoPrenotaEsame` varchar(16) DEFAULT NULL,
  `CodFiscaleUtentePrenotaEsame` varchar(16) DEFAULT NULL,
  `DataEOra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prenotazione`
--

INSERT INTO `prenotazione` (`IDPrenotazione`, `IDEsame`, `PartitaIVAClinica`, `Tipo`, `Confermata`, `Eseguita`, `CodFiscaleUtenteEffettuaEsame`, `CodFiscaleMedicoPrenotaEsame`, `CodFiscaleUtentePrenotaEsame`, `DataEOra`) VALUES
(1, 1, '12345', 'M', NULL, NULL, 'dmntnna89s42g438', 'dmrcld89s42g438s', NULL, '2016-04-26 09:25:54'),
(2, 1, '12345', 'M', 0, 0, 'mntrcr89h21a488l', 'dmrcld89s42g438s', NULL, '2016-04-27 15:03:40'),
(3, 2, '12346', 'U', 0, NULL, 'mntrcr89h21a488l', NULL, 'mntrcr89h21a488l', '2016-04-29 12:00:00'),
(5, 2, '12345', 'U', 1, 0, 'rndndt56s53t657o', NULL, 'rndndt56s53t657o', '2016-04-28 08:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `referto`
--

CREATE TABLE `referto` (
  `IDReferto` int(11) NOT NULL,
  `IDPrenotazione` int(11) DEFAULT NULL,
  `IDEsame` int(11) DEFAULT NULL,
  `PartitaIVAClinica` varchar(20) DEFAULT NULL,
  `Contenuto` longblob NOT NULL,
  `MedicoReferto` varchar(40) NOT NULL,
  `DataReferto` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `referto`
--

INSERT INTO `referto` (`IDReferto`, `IDPrenotazione`, `IDEsame`, `PartitaIVAClinica`, `Contenuto`, `MedicoReferto`, `DataReferto`) VALUES
(1, 1, 1, '12345', 0x696c207265666572746f20, 'Riga', '2016-04-26');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `CodFiscale` varchar(16) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Via` varchar(20) NOT NULL,
  `NumCivico` smallint(6) DEFAULT NULL,
  `CAP` mediumint(7) UNSIGNED NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Username` varchar(10) NOT NULL,
  `Password` varchar(10) NOT NULL,
  `PEC` varchar(100) NOT NULL,
  `CodFiscaleMedico` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`CodFiscale`, `Nome`, `Cognome`, `Via`, `NumCivico`, `CAP`, `Email`, `Username`, `Password`, `PEC`, `CodFiscaleMedico`) VALUES
('dmntnna89s42g438s', ' anna', ' di matteo', ' acquaventina', 30, 32767, ' annadima@alice.it','annadim', ' anna', ' annadima@pec.it', ' dmrcld89s42g438s\r'),
('mntrcr89h21a488l', 'riccardo', 'mantini', 'del carmine', 31, 6403, 'onizuka-89@hotmail.it', 'ricman','ricman', 'ricman@pec.it', 'dmrcld89s42g438s'),
('rndndt56s53t657o', 'rnd', 'ndt', 'bologna', 3, 32767, 'rnd@libero.it', 'rintintin','rnd', 'rnd@pec.it', 'dmrcld89s42g438s');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `clinica`
--
ALTER TABLE `clinica`
  ADD PRIMARY KEY (`PartitaIVA`),
  ADD UNIQUE KEY `NomeClinica` (`NomeClinica`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Username` (`Username`),
  ADD UNIQUE KEY `PEC` (`PEC`),
  ADD UNIQUE KEY `Telefono` (`Telefono`);

--
-- Indici per le tabelle `esame`
--
ALTER TABLE `esame`
  ADD PRIMARY KEY (`IDEsame`,`PartitaIVAClinica`),
  ADD KEY `PartitaIVAClinica` (`PartitaIVAClinica`);

--
-- Indici per le tabelle `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`CodFiscale`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Username` (`Username`),
  ADD UNIQUE KEY `PEC` (`PEC`),
  ADD UNIQUE KEY `NumIscrizione` (`NumIscrizione`);

--
-- Indici per le tabelle `prenotazione`
--
ALTER TABLE `prenotazione`
  ADD PRIMARY KEY (`IDPrenotazione`,`IDEsame`,`PartitaIVAClinica`),
  ADD KEY `IDEsame` (`IDEsame`),
  ADD KEY `PartitaIVAClinica` (`PartitaIVAClinica`),
  ADD KEY `CodFiscaleUtenteEffettuaEsame` (`CodFiscaleUtenteEffettuaEsame`),
  ADD KEY `CodFiscaleMedicoPrenotaEsame` (`CodFiscaleMedicoPrenotaEsame`),
  ADD KEY `CodFiscaleUtentePrenotaEsame` (`CodFiscaleUtentePrenotaEsame`);

--
-- Indici per le tabelle `referto`
--
ALTER TABLE `referto`
  ADD PRIMARY KEY (`IDReferto`),
  ADD KEY `IDPrenotazione` (`IDPrenotazione`),
  ADD KEY `IDEsame` (`IDEsame`),
  ADD KEY `PartitaIVAClinica` (`PartitaIVAClinica`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`CodFiscale`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Username` (`Username`),
  ADD UNIQUE KEY `PEC` (`PEC`),
  ADD KEY `CodFiscaleMedico` (`CodFiscaleMedico`);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `esame`
--
ALTER TABLE `esame`
  ADD CONSTRAINT `esame_ibfk_2` FOREIGN KEY (`PartitaIVAClinica`) REFERENCES `clinica` (`PartitaIVA`);

--
-- Limiti per la tabella `prenotazione`
--
ALTER TABLE `prenotazione`
  ADD CONSTRAINT `prenotazione_ibfk_1` FOREIGN KEY (`IDEsame`) REFERENCES `esame` (`IDEsame`),
  ADD CONSTRAINT `prenotazione_ibfk_2` FOREIGN KEY (`PartitaIVAClinica`) REFERENCES `clinica` (`PartitaIVA`),
  ADD CONSTRAINT `prenotazione_ibfk_3` FOREIGN KEY (`CodFiscaleUtenteEffettuaEsame`) REFERENCES `utente` (`CodFiscale`),
  ADD CONSTRAINT `prenotazione_ibfk_4` FOREIGN KEY (`CodFiscaleMedicoPrenotaEsame`) REFERENCES `medico` (`CodFiscale`),
  ADD CONSTRAINT `prenotazione_ibfk_5` FOREIGN KEY (`CodFiscaleUtentePrenotaEsame`) REFERENCES `utente` (`CodFiscale`);

--
-- Limiti per la tabella `referto`
--
ALTER TABLE `referto`
  ADD CONSTRAINT `referto_ibfk_1` FOREIGN KEY (`IDPrenotazione`) REFERENCES `prenotazione` (`IDPrenotazione`),
  ADD CONSTRAINT `referto_ibfk_2` FOREIGN KEY (`IDEsame`) REFERENCES `esame` (`IDEsame`),
  ADD CONSTRAINT `referto_ibfk_3` FOREIGN KEY (`PartitaIVAClinica`) REFERENCES `clinica` (`PartitaIVA`);

--
-- Limiti per la tabella `utente`
--
ALTER TABLE `utente`
  ADD CONSTRAINT `utente_ibfk_1` FOREIGN KEY (`CodFiscaleMedico`) REFERENCES `medico` (`CodFiscale`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
